---
layout: handbook-page-toc
title: "Swag: Process & FAQ"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Swag workflows

## Swag Order Process overview

1. GitLab will requests swag orders through emailing ssporer@boundlessnetwork.com.
1. Boundless recommends planning swag requests 2.5 weeks in advance to order, especially for international shipping because there can be issues with customs.
1. Provide the basic order requirements, optionally with the requirement of a custom notecard or a dedicated redemption page.
2. Boundless places the order so that's in the inventory. They also provides a spreadsheet of redemption codes and the URL for where individuals can redeem their codes (it may be an existing page since we have one for contributors and one for heroes).
3. GitLab shares the emails of winners/contributors.
4. Boundless sends out emails. As codes are redeemed, they update a shared spreadsheet with redemption status and swag delivery status.
5. Billings happens per invoice, but Boundless would prefer if we set up a monthly or quarterly PO instead. 

### Basic order

* Quantity of items
* Date by which the inventory needs to be fulfilled
* Redemption type: one code for all redemptions or multiple unique codes

### Option custom notecards

You can request to add custom notecards to kits.

* Boundless needs the verbiage to print on the card
* generally 3 x 5” with a GitLab logo on top

### New Redemption pages

If you would like to requesting a new page creation (one code for all redemptions), please provide our contact person with the following:

* Name of Page
* Name of Bundle
* Items in bundle
* Quantity
* Redemption Code (e.g HeroThnx23)

## Inventory store

There's an inventory store on brightsites where everything we've ordered is in stock and ready to ship. Given the right access, you can log in and order yourself. Access can be requested through an access request.
Purpose is for any items you want to purchased and that are in stock, ready to ship and stored at the warehouse. 

<https://gitlabstore.mybrightsites.com/>

## External access to swag stores

There is a possibility to create access for, say, Meetup organizers, to self-service their required swag within our budget. Please file an access request and assign this to your manager.

## Existing Redemption pages

### Heroes Program

<https://gitlabheroes.orderpromos.com/>

Redemption codes: <https://docs.google.com/spreadsheets/d/1ujtSUJsoFHmtF3NC6bbglNI1zXsojRQfCCLTCK8Uksc/edit#gid=0>

## Swag Nomination

If you think that a community member, contributor, or GitLab team memeber deserves a prize, [nominate them for some swag](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/community-appreciation/#nomination-process)! This also applies if you've reached an important milestone with your contributor (e.g First MR merged).

## Additional resources

- [Merchandise handling](/handbook/marketing/brand-and-product-marketing/brand/merchandise-handling/)
- [Field Marketing](/handbook/marketing/field-marketing/)
- [Email templates for sending swag links](/handbook/marketing/community-relations/code-contributor-program/community-appreciation/#templates-for-sending-swag-links)
- [Twitter templates for sending swag links](/handbook/marketing/community-relations/code-contributor-program/community-appreciation/#twitter-templates)
- [Finding Community Member Contact information](/handbook/marketing/community-relations/code-contributor-program/community-appreciation/#finding-community-member-contact-information)

## FAQ

### Is there any reason why we would need to follow up if someone uses their coupon?

We will need a way to verify that swag has been expensed, to verify that our swag budget is not exceeded, or calculate how much budget is left i.e. for quarterly reports.

### Can we not let contributors decide what they want from our store?

The price range in the swag store is 1$ to 100$ dollars. Using bundles with specific swag items for different outlets will help with yearly budget planning. Today we do not hand out blank vouchers with purely a monetary amount.

### Is there a documented process how to request new codes?

There is not a SSOT on this yet. Stay tuned. Do not email Savanah yourselves. Create an issue within Community Relations project and assign to @sugeroverflow (Fatima)

### Is there a documented process how to send codes to contributors?

No, that's something we should work on documenting. In the past we often emailed contributors with codes directly or Boundless emailing the contributors (once we provided the emails).

### Can we not have a portal through which we self-service codes? Or have a list of, say, 200 codes with different tiers of value?

It seems like we must request codes through Boundless because of how their systems are set up.

### Is this not always the same url?

No, they create a url for specific programs.

### Is there any reason why we would need to follow up if someone uses their coupon?

Boundless does the following up and updates the spreadsheet to let us know if the coupon was redeemed - which means the individual ordered swag. We need to make sure it gets delivered. They had some issues with deliveries recently - getting stuck at customs, etc. Sometimes it costs more than planned so they have to follow up and let us know if there's additional invoicing.

### I don't understand why we should plan ahead.

This is a strong request from Boundless. They need to move orders into the inventory so they need us to plan ahead and request number of items, which items, and by which date. After that, we can let contributors decide what to get. 

If you'd like to make it easier, you could plan ahead for a lot of items, pay for the invoice, and then create a page of all the items where contributors can self service.

If we give Boundless the emails of who to send codes to, they have offered to handle everything from there - sending the emails, keeping track of redemption, updating delivery status, etc in the spreadsheet.

### Can we switching to blank vouchers instead of specific bundle kits?

Boundless would like to move a monthly or quarterly PO on Coupa so they can bill more efficiently. If we do, we might also be able to do blank vouchers:

* Order a bunch of different items for the quarter
* Boundless will move the stock into the inventory store
* Boundless will generate custom page url and codes for a specific $ amount
* We will send them emails for Contributors
* They will send those contributors the codes and track redemption/delivery in a spreadsheet.

You can still do this in the current process. The only way this would be different is that instead of ordering for specific events e.g thank you heroes or hackathon Q3/Q4, we'd be ordering a general amount of stock.